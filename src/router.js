import Vue from 'vue'
import VueRouter from "vue-router"

Vue.use(VueRouter)

export default new VueRouter({

  mode: 'history',

  routes: [
    {
      path: '*', component: () => import('./components/NotFound')},
    {
      path: '/',
      redirect: '/dashboard'
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('./components/Dashboard'),
      meta: {
        title: 'Dashboard'
      },
    },
    {
      path: '/wells',
      name: 'wells',
      component: () => import('./components/well/Wells'),
      meta: {
        title: 'Well List'
      }
    },
    {
      path: '/well/create',
      name: 'create well',
      component: () => import('./components/well/Form'),
      meta: {
        title: 'create Well'
      }
    },
    {
      path: '/well/update/:id',
      name: 'update well',
      component: () => import('./components/well/Form'),
      meta: {
        title: 'Update Well'
      }
    }
  ]

})
