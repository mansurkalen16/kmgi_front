import Vue from 'vue'
import router from "./router";
import App from './components/App.vue'
import axios from 'axios'
import VueAWN from "vue-awesome-notifications"

Vue.use(VueAWN)

window.axios = axios
axios.defaults.baseURL = process.env.VUE_APP_URL

Vue.config.productionTip = false

String.prototype.capitalize = () => {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

new Vue({
  components: {
    App
  },
  router,
  render: h => h(App),
}).$mount('#app')
