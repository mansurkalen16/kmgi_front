import { Bar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
    extends: Bar,
    props: ['chartData'],
    mixins: [reactiveProp],
    mounted () {
        let options =  {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: true
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: true
            },
            tooltips: {
                enabled: true,
                mode: 'single',
                callbacks: {
                    label: function(tooltipItems) {
                        return tooltipItems.yLabel;
                    }
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            height: 200
        };

        this.renderChart(this.chartData, options)
    }
}
