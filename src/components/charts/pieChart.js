import { Pie, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
    extends: Pie,
    mixins: [reactiveProp],
    props: ['options'],
    watch: {
        chartData () {
            this.$data._chart.update()
        }
    },
    mounted () {
        this.renderChart(this.chartData, this.options)
    }
}
